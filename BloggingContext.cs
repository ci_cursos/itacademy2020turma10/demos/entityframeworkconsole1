using Microsoft.EntityFrameworkCore;
namespace entityframeworkconsole1
{
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs {get; set;}
        public DbSet<Post> Posts {get; set;}
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
            .UseSqlite("Data Source=blogging.db");
        }
    }
}