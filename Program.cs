﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace entityframeworkconsole1
{
    class Program
    {
        static void Main(string[] args)
        {
            using(var db = new BloggingContext())
            {   
                /*
                Console.WriteLine("Inserindo blog:");
                db.Add(new Blog{Url="http://umnovoblog.br/funciona"});
                db.Blogs.Add(new Blog{Url="http://maisoutroblog.br/funciona"});
                db.SaveChanges();
                */
                /*
                Console.WriteLine("Removendo blog:");
                var blog = db.Blogs.Find(1);
                db.Remove(blog);
                db.SaveChanges();
                */
                /*
                Console.WriteLine("Alterando blog:");
                var blog = db.Blogs.Find(2);
                blog.Url = "http://alterada";
                blog.Posts.Add(new Post{
                    Title = "Novidades!",
                    Content = "Um novo post do blog"
                });
                db.SaveChanges();
                */
                Console.WriteLine("Blogs:");
                /*
                //Eager
                var blogs = db.Blogs
                            .Include(blog => blog.Posts)
                            .ToList();
                foreach(var b in blogs)
                {
                    Console.WriteLine($"{b.BlogId} - {b.Url} - Posts={b.Posts.Count}");
                }
                */
                /*
                //Explicit
                var blog = db.Blogs.Single(blog => blog.BlogId == 2);
                Console.WriteLine($"{blog.BlogId} - {blog.Url} - Posts={blog.Posts.Count}");
                db.Entry(blog).Collection(b => b.Posts).Load();
                Console.WriteLine($"{blog.BlogId} - {blog.Url} - Posts={blog.Posts.Count}");
                */
                
            }
        }
    }
}
